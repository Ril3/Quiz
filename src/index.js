// Select all elements

let start = document.getElementById("start");
let quiz = document.getElementById("quiz");
let question = document.getElementById("question");
let qImg = document.getElementById("qImg");
let choiceA = document.getElementById("A");
let choiceB = document.getElementById("B");
let choiceC = document.getElementById("C");
let counter = document.getElementById("counter");
let timeGauge = document.getElementById("timeGauge");
let progress = document.getElementById("progress");
let scoreDiv = document.getElementById("scoreContainer");




// Create questions

let questions = [
  {
    question : "Dans quel pays se trouve la ville d'Ancône?",
    imgSrc : "img/ancona.jpg",
    choiceA : "A: Italie",
    choiceB : "B: Espagne",
    choiceC : "C: Grèce",
    correct : "A"
},{
    question : "Qui a écrit les livres de Harry Potter?",
    imgSrc : "img/harry.jpg",
    choiceA : "A: C.S. Lewis",
    choiceB : "B: J.K. Rowling",
    choiceC : "C: Suzanne Collins",
    correct : "B"
},{
    question : "Superman, Batman et Spiderman sont les super-héros les plus célèbres. Lequel a obtenu le film en premier?",
    imgSrc : "img/heroes.jpg",
    choiceA : "A: Spiderman",
    choiceB : "B: Batman",
    choiceC : "C: Superman",
    correct : "C"
},{
  question : "Quelle ville est connue comme la ville de lumière?",
  imgSrc : "img/paris.jpg",
  choiceA : "A: Londres",
  choiceB : "B: Los Angeles",
  choiceC : "C: Paris",
  correct : "C"
},{
  question : "Quelle planète est la plus chaude du système solaire?",
  imgSrc : "img/venera.jpg",
  choiceA : "A: Vénus",
  choiceB : "B: Mars",
  choiceC : "C: Mercure",
  correct : "A"
},{
  question : "Quels sont les deux pays qui partagent la plus longue frontière internationale?",
  imgSrc : "img/canada.jpg",
  choiceA : "A: Russie et Ukraine",
  choiceB : "B: Canada et États-Unis",
  choiceC : "C: Norvège et Suède",
  correct : "B"
},{
  question : "Quel animal peut nettoyer son oreille avec sa langue?",
  imgSrc : "img/animals.jpg",
  choiceA : "A: Singe",
  choiceB : "B: Éléphant",
  choiceC : "C: Girafe",
  correct : "C"
},{
question : "Quel fruit a des graines à l'extérieur?",
imgSrc : "img/frut.jpg",
choiceA : "A: Mûre",
choiceB : "B: Framboise",
choiceC : "C: Fraise",
correct : "C"
}, {
  question : "Combien de cercles le logo olympique officiel comporte-t-il?",
  imgSrc : "img/olimp.jpg",
  choiceA : "A: 5",
  choiceB : "B: 3",
  choiceC : "C: 7",
  correct : "A"
},{
  question : "Qui a été le premier homme dans l'espace?",
  imgSrc : "img/space.jpg",
  choiceA : "A: Sergei Krikalev",
  choiceB : "B: Yuri Gagarin",
  choiceC : "C: Neil Armstrong",
  correct : "B"
},{
  question : "La plus grande pyramide d'Egypte est?",
  imgSrc : "img/pira.jpg",
  choiceA : "A: Pyramide de Menkor",
  choiceB : "B: Pyramide de Kefner",
  choiceC : "C: Pyramide de Khéops",
  correct : "C"
},{
question : "Dans quel club David Beckham a-t-il mis fin à sa carrière?",
imgSrc : "img/david.jpg",
choiceA : "A: Manchester United",
choiceB : "B: Real Madrid",
choiceC : "C: Paris Saint-Germain",
correct : "C"
}, {
  question : "Où les billets en papier ont-ils commencé à être utilisés pour la première fois?",
  imgSrc : "img/pare.jpg",
  choiceA : "A: Chine",
  choiceB : "B: Inde",
  choiceC : "C: Portugal",
  correct : "A"
},{
  question : "Quel est le plus gros animal du monde?",
  imgSrc : "img/animals.jpg",
  choiceA : "A: Anaconda vert",
  choiceB : "B: Baleine bleue",
  choiceC : "C: Éléphant",
  correct : "B"
},{
  question : "Marie Antoinette l'était?",
  imgSrc : "img/kraljica.jpg",
  choiceA : "A: Chanteuse d'opéra",
  choiceB : "B: Reine d'Angleterre",
  choiceC : "C: Reine de France",
  correct : "C"
},{
question : "Quel est le lac le plus profond du monde?",
imgSrc : "img/lac.jpg",
choiceA : "Lac Caspienne",
choiceB : "Lac Victoria",
choiceC : "Lac Baïkal",
correct : "C"
}

];



// Create variables

const lastQuestion = questions.length - 1;
let runningQuestion = 0;
let count = 0;
const questionTime = 10; // 10s
const gaugeWidth = 150; // 150px
const gaugeUnit = gaugeWidth / questionTime;
let TIMER;
let score = 0;



// Render a question

function renderQuestion(){
  let q = questions[runningQuestion];
  
  question.innerHTML = "<p>"+ q.question +"</p>";
  qImg.innerHTML = "<img src="+ q.imgSrc +">";
  choiceA.innerHTML = q.choiceA;
  choiceB.innerHTML = q.choiceB;
  choiceC.innerHTML = q.choiceC;
}



  // Event listener on click to start the quiz
  start.addEventListener("click", startQuiz);




  // Start quiz

  function startQuiz(){
    start.style.display = "none";
    renderQuestion();
    quiz.style.display = "flex";
    renderProgress();
    renderCounter();
    TIMER = setInterval(renderCounter,1000); // 1000ms = 1s
}



  // Render progress

  function renderProgress(){
    for(let qIndex = 0; qIndex <= lastQuestion; qIndex++){
        progress.innerHTML += "<div class='prog' id="+ qIndex +"></div>";
    }
  }



  // Counter render

  function renderCounter(){
    if(count <= questionTime){
        counter.innerHTML = count;
        timeGauge.style.width = count * gaugeUnit + "px";
        count++
    }else{
        count = 0;
        // change progress color to red
        answerIsWrong();

          if(runningQuestion < lastQuestion){
              runningQuestion++;
              renderQuestion();
          }else{
              // end the quiz and show the score
              clearInterval(TIMER);
              scoreRender();
        }
    }
}



  // Check Answer 
  function checkAnswer(answer){
    if( answer == questions[runningQuestion].correct){
        // answer is correct
        score++;
        // change progress color to green
        answerIsCorrect();
    }else{
        // answer is wrong
        // change progress color to red
        answerIsWrong();
    }
    count = 0;
    if(runningQuestion < lastQuestion){
        runningQuestion++;
        renderQuestion();
    }else{
        // end the quiz and show the score
        clearInterval(TIMER);
        scoreRender();
    }
}



  // Answer is correct
  
  function answerIsCorrect(){
    document.getElementById(runningQuestion).style.background = "radial-gradient(50% 50% at 50% 50%, rgba(5, 140, 11, 0) 0%, #238700 72.92%)";
}



  // Answer is wrong
  
  function answerIsWrong(){
    document.getElementById(runningQuestion).style.background = "radial-gradient(50% 50% at 50% 50%, rgba(255, 0, 0, 0) 0%, #BD0000 72.92%)";
}



    // Score render 

    function scoreRender(){
      scoreDiv.style.display = "flex";
      
      // calculate the amount of question percent answered by the user
      const scorePerCent = Math.round(100 * score/questions.length);
      
      // choose the image based on the scorePerCent
      let img = (scorePerCent >= 80) ? "img/5.png" :
                (scorePerCent >= 60) ? "img/4.png" :
                (scorePerCent >= 40) ? "img/3.png" :
                (scorePerCent >= 20) ? "img/2.png" :
                "img/1.png";
      
      scoreDiv.innerHTML = "<img src="+ img +">";
      scoreDiv.innerHTML += "<p>"+ scorePerCent +"%</p>";
  }